#/bin/env/python2.7
import re
import sys
import json
import signal
import argparse
from lxml import etree
from jnpr.junos import Device
from contextlib import contextmanager
from jnpr.junos.utils.config import Config

rawstr = r"""not\s+found"""


@contextmanager
def timeout(host, s=15):
    def exit(signum, frame, host=host):
        raise Exception("Timeout error for joining host %s" % host)
    try:
        signal.signal(signal.SIGALRM, exit)
        signal.alarm(s)
        yield
    finally:
        signal.alarm(0)


class Device_junos():

    def __init__(self, vm, **kwconnect):
        try:
            with timeout(kwconnect['host']):
                dev = Device(**kwconnect)
                dev.open()
                dev.bind(cu=Config)
        except Exception as err:
            print(json.dumps({'error': str(err)}))
            sys.exit(2)
        self.vm = vm
        self.dev = dev
        self._vlanid = self.vm['address'].split('.')[1][-1] + self.vm['address'].split('.')[2]

    @property
    def cmds(self):
        cmds = [
            "set interfaces ae0 unit {} description {}".format(
                self._vlanid, self.vm['project_name']),
            "set interfaces ae0 unit {0} vlan-id {0}".format(self._vlanid),
            "set interfaces ae0 unit {} family inet address {}/{}".format(
                self._vlanid, self.vm['gateway'], self.vm['mask_prefix']
            ),
            "set security zones security-zone CLIENTS-VM interfaces ae0.{} host-inbound-traffic system-services ping".format(self._vlanid),
            "set security zones security-zone CLIENTS-VM address-book address {} {}/32".format(self.vm['hostname'], self.vm['address']),
            "set security policies from-zone PUBLIC-BACKBONE to-zone CLIENTS-VM policy PUBLIC-BACKBONE-TO-CLIENTS-VM-{} match source-address any".format(self.vm['hostname']),
            "set security policies from-zone PUBLIC-BACKBONE to-zone CLIENTS-VM policy PUBLIC-BACKBONE-TO-CLIENTS-VM-{0} match destination-address {0}".format(self.vm['hostname']),
            "set security policies from-zone PUBLIC-BACKBONE to-zone CLIENTS-VM policy PUBLIC-BACKBONE-TO-CLIENTS-VM-{} match application junos-http".format(self.vm['hostname']),
            "set security policies from-zone PUBLIC-BACKBONE to-zone CLIENTS-VM policy PUBLIC-BACKBONE-TO-CLIENTS-VM-{} match application junos-https".format(self.vm['hostname']),
            "set security policies from-zone PUBLIC-BACKBONE to-zone CLIENTS-VM policy PUBLIC-BACKBONE-TO-CLIENTS-VM-{} then permit".format(self.vm['hostname']),
            "set security nat destination rule-set NAT-HOSTING rule {} match destination-address {}/32".format(self.vm['hostname'], self.vm['address_ip_public']),
            "set security nat destination rule-set NAT-HOSTING rule {0} then destination-nat pool {0}".format(self.vm['hostname']),
            "set security nat destination pool {} address {}/32".format(self.vm['hostname'], self.vm['address'])
        ]
        return cmds

    @property
    def vlanid(self):
        return self._vlanid

    def __enter__(self):
        return self

    def load(self, set_cmd, format='set'):
        self.dev.cu.load(set_cmd, format=format)

    def __exit__(self, *exc):
        self.dev.close()


def main(args):
    vm = vars(args)
    kwargs = {
        'host': args.junos[0],
        'user': args.junos[1],
        'password': args.junos[2],
        'port': args.port
    }
    with Device_junos(vm, **kwargs) as d:
        matchstr = etree.tostring(
            d.dev.rpc.get_interface_information(
                interface_name='ae0.{}'.format(d.vlanid)
            )
        )
        vlan_not_found = re.search(rawstr, matchstr, re.MULTILINE)
        if vlan_not_found:
            try:
                for cmd in d.cmds:
                    d.load(set_cmd=cmd)
            except Exception:
                d.dev.cu.rollback()
        else:
            try:
                for cmd in d.cmds[4:]:
                    d.load(set_cmd=cmd)
            except Exception:
                d.dev.cu.rollback()
        if d.dev.cu.commit():
            print json.dumps(
                {
                    'data': 'success for set firewall policies for vm {}'.format(
                        d.vm['hostname']
                    )
                }
            )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Junos : set parameters for vm'
    )
    parser.add_argument(
        'hostname',
        help='vm hostname',
        metavar='hostname'
    )
    parser.add_argument(
        'address',
        help='vm address',
        metavar='address',
    )
    parser.add_argument(
        'mask_prefix',
        help='vm mask_prefix',
        metavar='mask_prefix',
    )
    parser.add_argument(
        'gateway',
        help='vm gateway',
        metavar='gateway'
    )
    parser.add_argument(
        'project_name',
        help='vm project_name',
        metavar='project_name',
    )
    parser.add_argument(
        'address_ip_public',
        help='vm address_ip_public',
        metavar='address_ip_public',
    )
    parser.add_argument(
        '--junos',
        help='srx identifiers for connection',
        metavar=('ip_or_hostname', 'user', 'password'),
        dest='junos',
        type=str,
        nargs=3,
        default=['10.250.95.150', 'root', 'toto1234']
    )
    parser.add_argument(
        '--port', '-p',
        help='port netconf',
        metavar='num_port',
        type=int,
        default=22
    )
    args = parser.parse_args()
    main(args)
